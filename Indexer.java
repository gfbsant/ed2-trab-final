import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Indexer {

	/*
	 * indexer --freq N ARQUIVO
	 * indexer --freq-word PALAVRA ARQUIVO
	 * indexer --search TERMO ARQUIVO [ARQUIVO ...]
	 */

	public static void main(String[] args) {

		String comando = args[0];

		switch (comando) {
			case "--freq":
				int quantidade = Integer.parseInt(args[1]);
				exibirPalavrasMaisFrequentes(quantidade, args[2]);
				break;
			case "--freq-word":
				String palavra = args[1];
				pesquisarFrequenciaPalavras(palavra, args[2]);
				break;
			case "--search":
			List<String> termosBusca = new ArrayList<>();
			for (int i = 1; i < args.length; i++) {
				if (args[i].endsWith(".txt")) {
					break;
				}
					if (!args[i].contains("\"")) {
					termosBusca.add(args[i].toLowerCase());
				} else {
					String[] palavras = args[i].toLowerCase().replace("\"", "").split(" ");
					termosBusca.addAll(Arrays.asList(palavras));
				}
			}
				String[] arquivos = Arrays.copyOfRange(args, termosBusca.size() + 1, args.length);
				buscarArquivosPorTermo(termosBusca, arquivos);
				break;
			default:
				System.out.println("Argumento inválido: " + comando);
				break;
		}

	}

	public static void exibirPalavrasMaisFrequentes(int n, String arquivo) {

		if (fileNotFound(arquivo)) {
			System.out.println("O arquivo " + arquivo + " não existe ou não é um arquivo válido.");
			return;
		}

		try {
			BufferedReader br = new BufferedReader(new FileReader(arquivo));
			Map<String, Integer> frequenciaPalavras = new HashMap<>();

			String linha;
			while ((linha = br.readLine()) != null) {
				String[] palavras = linha.split("\\s+|(?=\\p{Punct})|(?<=\\p{Punct})");

				for (String palavra : palavras) {
					palavra = palavra.replaceAll("^\\p{Punct}+|\\p{Punct}+$", "");
					palavra = palavra.toLowerCase();
					if (palavra.length() >= 2 && palavra.matches(".*[a-zA-Z].*")) {
						if (frequenciaPalavras.containsKey(palavra)) {
							int frequencia = frequenciaPalavras.get(palavra);
							frequenciaPalavras.put(palavra, frequencia + 1);
						} else {
							frequenciaPalavras.put(palavra, 1);
						}
					}
				}
			}
			br.close();

			List<Map.Entry<String, Integer>> listaOrdenada = new ArrayList<>(frequenciaPalavras.entrySet());
			listaOrdenada.sort((a, b) -> b.getValue().compareTo(a.getValue()));

			System.out.println("As " + n + " palavras mais frequentes no arquivo são:");
			for (int i = 0; i < n && i < listaOrdenada.size(); i++) {
				Map.Entry<String, Integer> entrada = listaOrdenada.get(i);
				System.out.println(entrada.getKey() + ": " + entrada.getValue() + " vezes");
			}
		} catch (IOException e) {
			System.err.println("Erro de leitura do arquivo: " + e.getMessage());
		}
	}

	public static void pesquisarFrequenciaPalavras(String targetWord, String arquivo) {

		if (fileNotFound(arquivo)) {
			System.out.println("O arquivo " + arquivo + " não existe ou não é um arquivo válido.");
			return;
		}

		try {
			Map<String, Integer> frequenciaPalavras = new HashMap<>();
			BufferedReader br = new BufferedReader(new FileReader(arquivo));
			String linha;

			while ((linha = br.readLine()) != null) {
				String[] palavras = linha.split("\\s+|(?=\\p{Punct})|(?<=\\p{Punct})");

				for (String palavra : palavras) {
					palavra = palavra.replaceAll("^\\p{Punct}+|\\p{Punct}+$", "");
					palavra = palavra.toLowerCase();
					if (palavra.length() >= 2 && palavra.matches(".*[a-zA-Z].*")) {
						if (frequenciaPalavras.containsKey(palavra)) {
							int frequencia = frequenciaPalavras.get(palavra);
							frequenciaPalavras.put(palavra, frequencia + 1);
						} else {
							frequenciaPalavras.put(palavra, 1);
						}
					}
				}
			}

			br.close();

			int frequencia = frequenciaPalavras.getOrDefault(targetWord.toLowerCase(), 0);
			System.out.println("A palavra \"" + targetWord + "\" aparece " + frequencia + " vezes no arquivo.");
			return;

		} catch (IOException e) {
			System.out.println("Erro ao ler o arquivo: " + e.getMessage());
		}
		return;
	}

	public static void buscarArquivosPorTermo(List<String> termosBusca, String[] arquivos) {
		try {
			ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
			ConcurrentHashMap<String, Map<String, Double>> tfidfMap = new ConcurrentHashMap<>();

			for (String arquivo : arquivos) {
				if (fileNotFound(arquivo)) {
					System.out.println("O arquivo " + arquivo + " não existe ou não é um arquivo válido.");
					return;
				}
				executor.submit(() -> {
					if (fileNotFound(arquivo)) {
						System.out.println("O arquivo " + arquivo + " não existe ou não é um arquivo válido.");
						return;
					}

					Map<String, Integer> frequenciaPalavras = new HashMap<>();
					double totalPalavras = 0;

					try (BufferedReader br = new BufferedReader(new FileReader(arquivo))) {
						String linha;

						while ((linha = br.readLine()) != null) {
							String[] palavras = linha.split("\\s+|(?=\\p{Punct})|(?<=\\p{Punct})");

							for (String palavra : palavras) {
								palavra = palavra.replaceAll("^\\p{Punct}+|\\p{Punct}+$", "");
								palavra = palavra.toLowerCase();
								if (palavra.length() >= 2 && palavra.matches(".*[a-zA-Z].*")) {
									totalPalavras++;
									frequenciaPalavras.merge(palavra, 1, Integer::sum);
								}
							}
						}
					} catch (IOException e) {
						e.printStackTrace();
					}

					Map<String, Double> tfMap = new HashMap<>();
					for (Map.Entry<String, Integer> entry : frequenciaPalavras.entrySet()) {
						double tf = entry.getValue() / totalPalavras;
						tfMap.put(entry.getKey(), tf);
					}

					tfidfMap.put(arquivo, tfMap);
				});
			}

			executor.shutdown();
			try {
				executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			Map<String, Double> idfMap = new HashMap<>();
			for (String termo : termosBusca) {
				int qtdDocumentosComTermo = 0;
				for (Map<String, Double> tfMap : tfidfMap.values()) {
					if (tfMap.containsKey(termo)) {
						qtdDocumentosComTermo++;
					}
				}
				double idf = Math.log((double) tfidfMap.size() / (double) (1 + qtdDocumentosComTermo));
				idfMap.put(termo, idf);
			}

			Map<String, Double> relevanciaArquivos = new HashMap<>();
			for (Map.Entry<String, Map<String, Double>> entry : tfidfMap.entrySet()) {
				double relevancia = 0;
				for (String termo : termosBusca) {
					if (entry.getValue().containsKey(termo)) {
						double tfidf = Math.abs(entry.getValue().get(termo) * idfMap.get(termo));
						tfidf = Double.parseDouble(new DecimalFormat("#.###").format(tfidf).replace(",", "."));
						relevancia += tfidf;
					}
				}
				relevanciaArquivos.put(entry.getKey(), relevancia);
			}

			List<Map.Entry<String, Double>> arquivosOrdenados = new ArrayList<>(relevanciaArquivos.entrySet());
			arquivosOrdenados.sort((a, b) -> b.getValue().compareTo(a.getValue()));

			System.out.println("Arquivos mais relevantes para os termos de busca " + termosBusca);
			for (Map.Entry<String, Double> entry : arquivosOrdenados) {
				System.out.println(entry.getKey() + " - Relevância: " + entry.getValue());
			}

		} catch (Exception e) {
			System.err.println("Erro ao ler o arquivo: " + e.getMessage());
		}
	}

	private static boolean fileNotFound(String arquivo) {
		File retorno = new File(arquivo);

		if (retorno.exists()) {
			return false;
		} else {
			return true;
		}
	}

}
